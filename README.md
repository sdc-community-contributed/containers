Singularity
===========
HBA
---
Singularity.lofar_hba_sksp_base - part 1 of the recipe for SKSP focussed images, slow moving dependencies, by Frits Sweijen.

Singularity.lofar_hba_sksp - part 2 of the recipe for SKSP focussed images, faster moving software, by Frits Sweijen.

To build a working container, first build the base container by running

    sudo singularity build lofar_hba_sksp_base.sif Singularity.lofar_hba_sksp_base

Then build the main container by first pointing the second recipe towards the base container using

    Bootstrap: localimage
    From: lofar_hba_sksp_base.sif

followed by

    sudo singularity build lofar_hba_sksp.sif Singularity.lofar_hba_sksp
